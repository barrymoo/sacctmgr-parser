{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Qos where

import           Control.Applicative        ((<|>))
import qualified Data.Text                  as T
import           Data.Void
import           Text.InterpolatedString.QM
import           Text.Megaparsec
import           Text.Megaparsec.Char
import           Text.Megaparsec.Char.Lexer

data Tres =
  Cpu Integer
  | Mem
    { _value :: Double
    , _unit  :: Char
    }
  | Gres Integer

instance Show Tres where
  show (Cpu x)     = [qms|"cpu={x}"|]
  show (Gres x)    = [qms|"gres/gpu={x}"|]
  show (Mem v 'T') = [qms|"mem={floor (v * 1024)}G"|]
  show (Mem v u)   = [qms|"mem={floor v}{u}"|]

data Qos =
  Qos
    { _name      :: T.Text
    , _priority  :: Integer
    , _maxtrespa :: [Tres]
    , _maxwall   :: T.Text
    }
  deriving (Show)

type Parser = Parsec Void T.Text

notBar :: Parser Char
notBar = satisfy (\x -> x /= '|' && x /= '\n')

name :: Parser T.Text
name = T.pack <$> some notBar

priority :: Parser Integer
priority = read <$> some notBar

tres :: Parser Tres
tres =     try cpu
       <|> try gres
       <|> try mem
  where
    cpu = fmap Cpu $ string "cpu=" *> decimal
    gres = fmap Gres $ string "gres/gpu=" *> decimal
    mem = Mem <$> value <*> upperChar
    value = string "mem=" *> (try float <|> fromInteger <$> decimal)

maxtrespa :: Parser [Tres]
maxtrespa = sepBy tres ","

maxwall :: Parser T.Text
maxwall = T.pack <$> some notBar

parseQos :: Parser Qos
parseQos =
  Qos <$> (name <* bar) <*> (priority <* bar) <*> (maxtrespa <* bar) <*> maxwall
  where
    bar = char '|'

parseQosLines :: Parser [Qos]
parseQosLines = endBy parseQos newline
