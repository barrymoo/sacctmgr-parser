{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module RunSacctmgr where

import qualified Data.Text                  as T
import           Qos
import           Shelly
import           Text.InterpolatedString.QM

newtype Sudo a =
  Sudo
    { sudo :: Sh a
    }

run_sudo :: T.Text -> [T.Text] -> Sudo T.Text
run_sudo cmd args = Sudo $ run "sudo" (cmd : args)

qosOutput :: Sh T.Text
qosOutput =
  run
    "sacctmgr"
    ["-P", "-n", "show", "qos", "format=name,priority,maxtrespa,maxwall"]

strip :: T.Text -> T.Text
strip = T.replace "\"" ""

intercalateMaxTresPa :: Show a => [a] -> T.Text
intercalateMaxTresPa = strip . t
 where
   t = T.intercalate "," . fmap (T.pack . show)

increaseTres :: Tres -> Tres
increaseTres (Cpu x) = Cpu $ floor (fromIntegral x * 1.1)
increaseTres (Gres x) = Gres $ floor (fromIntegral x * 1.1)
increaseTres (Mem v 'T') = Mem (v * 1.1) 'T'
increaseTres (Mem v u) = Mem (fromIntegral . floor $ v * 1.1) u

buildInvestQos :: Qos -> Sudo T.Text
buildInvestQos Qos {_name = n, _priority = p, _maxtrespa = mt, _maxwall = mw} =
  run_sudo
    "sacctmgr"
    [ "-i"
    , "add"
    , "qos"
    , strip [qms|"{n}-invest"|]
    , "where"
    , strip [qms|"priority={p}"|]
    , strip [qms|"maxtrespa={intercalateMaxTresPa $ increaseTres <$> mt}"|]
    , strip [qms|"maxwall={mw}"|]
    ]

modifyInvestQos :: Qos -> Sudo T.Text
modifyInvestQos Qos {_name = n, _priority = p, _maxtrespa = mt, _maxwall = mw} =
  run_sudo
    "sacctmgr"
    [ "-i"
    , "modify"
    , "qos"
    , "where"
    , strip [qms|"name={n}"|]
    , "set"
    , strip [qms|"priority={p}"|]
    , strip [qms|"maxtrespa={intercalateMaxTresPa $ increaseTres <$> mt}"|]
    , strip [qms|"maxwall={mw}"|]
    ]

deleteInvestQos :: Qos -> Sudo T.Text
deleteInvestQos Qos {_name = n, _priority = p, _maxtrespa = mt, _maxwall = mw} =
  run_sudo
    "sacctmgr"
    [ "-i"
    , "delete"
    , "qos"
    , "where"
    , strip [qms|"name={n}"|]
    ]

runBuildInvestQos :: Qos -> Sudo T.Text
runBuildInvestQos qos@Qos {_name = n, _priority = p, _maxtrespa = mt, _maxwall = mw} =
  case mt of
    [] -> run_sudo "echo" ["skipped"]
    _  -> buildInvestQos qos

runOnInvest :: (Qos -> Sudo T.Text) -> Qos -> Sudo T.Text
runOnInvest f qos@Qos {_name = n, _priority = p, _maxtrespa = mt, _maxwall = mw} =
  case T.isInfixOf "invest" n of
    True -> f qos
    _ -> run_sudo "echo" ["skipped"]

runModifyInvestQos :: Qos -> Sudo T.Text
runModifyInvestQos = runOnInvest modifyInvestQos

runDeleteInvestQos :: Qos -> Sudo T.Text
runDeleteInvestQos = runOnInvest deleteInvestQos
